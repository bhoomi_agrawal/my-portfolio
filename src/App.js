import React from "react";

function App() {
  return (
    <div>
      <article className="resume-wrapper text-center position-relative">
        <div className="resume-wrapper-inner mx-auto text-left bg-white shadow-lg">
          <header className="resume-header pt-4 pt-md-0">
            <div className="media flex-column flex-md-row">
              {/* <img
                class="mr-3 img-fluid picture mx-auto"
                src={"images/profile.jpg"}
                alt="vfffdfd  "
                style={{ height: "220px", width: "unset" }}
              /> */}
              <div className="media-body p-4 d-flex flex-column flex-md-row mx-auto mx-lg-0">
                <div className="primary-info">
                  <h1 className="name mt-0 mb-1 text-white text-uppercase text-uppercase">
                    Bhoomika Agrawal
                  </h1>
                  <div className="title mb-3">MERN STACK Developer</div>
                  <ul className="list-unstyled">
                    <li className="mb-2">
                      <a href="mailto:agrawalbhoomika16@gmail.com">
                        <i
                          className="far fa-envelope fa-fw mr-2"
                          data-fa-transform="grow-3"
                        />
                        agrawalbhoomika16@gmail.com
                      </a>
                    </li>
                    {/* <li>
                      <a href="tel:+917014867597">
                        <i
                          className="fas fa-mobile-alt fa-fw mr-2"
                          data-fa-transform="grow-6"
                        />
                        +917014867597
                      </a>
                    </li> */}
                  </ul>
                </div>
                {/*//primary-info*/}
                <div className="secondary-info ml-md-auto mt-2">
                  <ul className="resume-social list-unstyled">
                    <li className="mb-3">
                      <a href="https://www.linkedin.com/in/bhoomika-agrawal-a06aaa149/">
                        <span className="fa-container text-center mr-2">
                          <i className="fab fa-linkedin-in fa-fw" />
                        </span>
                        linkedin.com/in/bhoomika-agrawal-a06aaa149/
                      </a>
                    </li>
                    <li className="mb-3">
                      <a
                        href="https://www.github.com/bhoomiagrawal"
                        target="blank"
                      >
                        <span className="fa-container text-center mr-2">
                          <i className="fab fa-github-alt fa-fw" />
                        </span>
                        github.com/bhoomiagrawal
                      </a>
                    </li>
                    <li>
                      <a href="https://medium.com/@bhoomi_">
                        <span className="fa-container text-center mr-2">
                          <i className="fas fa-globe" />
                        </span>
                        https://medium.com/@bhoomi_
                      </a>
                    </li>
                  </ul>
                </div>
                {/*//secondary-info*/}
              </div>
              {/*//media-body*/}
            </div>
            {/*//media*/}
          </header>
          <div className="resume-body py-2 px-3">
            <section className="resume-section summary-section mb-2">
              <h2 className="resume-section-title text-uppercase font-weight-bold pb-2 mb-2">
                Career Summary
              </h2>
              <div className="resume-section-content">
                <p className="mb-0">
                  Courteous and Passionate front-end web developer with 4+ years
                  of experience using Javascript to build all aspects of the
                  user experience and user interface for client-facing landing
                  pages. Specializes in using ReactJs to build E-commerce sites.
                </p>
              </div>
            </section>
            {/*//summary-section*/}
            <div className="row">
              <div className="col-lg-9 col-sm-9">
                <section className="resume-section experience-section mb-5">
                  <h2 className="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">
                    Work Experience
                  </h2>
                  <div className="resume-section-content">
                    <div className="resume-timeline position-relative">
                      {/*//resume-timeline-item*/}

                      <article className="resume-timeline-item position-relative">
                        <div className="resume-timeline-item-header mb-2">
                          <div className="d-flex flex-column flex-md-row">
                            <h3 className="resume-position-title font-weight-bold mb-1">
                              Mernstack Developer
                            </h3>
                            <div className="resume-company-name ml-auto">
                              <a href="https://deorwine.com/" target="blank">
                                {" "}
                                Dotsquares Technologies India Pvt. Ltd.
                              </a>
                            </div>
                          </div>
                          {/*//row*/}
                          <div className="resume-position-time">
                            2022- Present
                          </div>
                        </div>
                        {/*//resume-timeline-item-header*/}
                        <div className="resume-timeline-item-desc">
                          <p>
                            Working in Dotsquares Technologies as Mernstack
                            Developer.
                          </p>
                          <h4 className="resume-timeline-item-desc-heading font-weight-bold">
                            My Projects :
                          </h4>
                          <ul className="list-unstyled mb-2">
                            <li className="mb-2 ">
                              <h6 className>
                                <a
                                  href="http://moonaru-react-frontend-admin-uat.s3-website-ap-southeast-2.amazonaws.com"
                                  target="blank"
                                >
                                  Moonaru:
                                </a>
                              </h6>
                            </li>
                            <ul className="list-unstyled mb-2">
                              <li>
                                Admin Panel for user management for moonaru
                                (video upload and share ios app) using React JS
                                and Node JS.
                              </li>
                            </ul>
                            <li className="mb-2 ">
                              <h6 className>
                                <a
                                  href="admin.bigpurplephone.com"
                                  target="blank"
                                >
                                  BigPurple:
                                </a>
                              </h6>
                            </li>
                            <ul className="list-unstyled mb-2">
                              <li>
                                Portal for manage old age family member phone
                                using React JS and Node JS.
                              </li>
                            </ul>
                          </ul>
                        </div>
                      </article>
                      <article className="resume-timeline-item position-relative">
                        <div className="resume-timeline-item-header mb-2">
                          <div className="d-flex flex-column flex-md-row">
                            <h3 className="resume-position-title font-weight-bold mb-1">
                              React JS Developer
                            </h3>
                            <div className="resume-company-name ml-auto">
                              <a href="https://deorwine.com/" target="blank">
                                {" "}
                                Deorwine Infotech
                              </a>
                            </div>
                          </div>
                          {/*//row*/}
                          <div className="resume-position-time">2018- 2022</div>
                        </div>
                        {/*//resume-timeline-item-header*/}
                        <div className="resume-timeline-item-desc">
                          <p>
                            Worked in Deorwine Infotech as React JS Developer.
                          </p>
                          <h4 className="resume-timeline-item-desc-heading font-weight-bold">
                            My Projects :
                          </h4>
                          <ul className="list-unstyled mb-2">
                            <li className="mb-2 ">
                              <h6 className>
                                <a
                                  href="https://www.blissbasket.com"
                                  target="blank"
                                >
                                  BlissBasket:
                                </a>
                              </h6>
                            </li>
                            <ul className="list-unstyled mb-2">
                              <li>
                                E-Commercial Website made in Next Js (Framework
                                of React JS).
                              </li>
                            </ul>

                            <li className="mb-2">
                              <h6 className>
                                <a
                                  href="http://kuwait-local.herokuapp.com/"
                                  target="blank"
                                >
                                  Kuwait Local
                                </a>
                              </h6>
                            </li>
                            <ul className="list-unstyled mb-2">
                              <li>
                                Kuwait's local news, events, classifieds,
                                services, exchange rates, gold rates, photos,
                                videos, businesses, every local things are on
                                one place. Created in Next JS
                              </li>
                            </ul>

                            <li className="mb-2">
                              <h6 className>
                                <a
                                  href="http://dev.flymychef.com/"
                                  target="blank"
                                >
                                  BydByt(B2C)
                                </a>
                              </h6>
                            </li>
                            <ul className="list-unstyled mb-2">
                              <li>Worked on this project in Next JS.</li>
                            </ul>

                            <li className="mb-2">
                              <h6 className>
                                <a
                                  href="http://dev-partner.flymychef.com/"
                                  target="blank"
                                >
                                  BydByt(B2B)
                                </a>
                              </h6>
                            </li>
                            <ul className="list-unstyled mb-2">
                              <li>Worked on this project in Next JS.</li>
                            </ul>

                            <li className="mb-2">
                              <h6 className>
                                <a href="javascript:void" target="blank">
                                  FegEx
                                </a>
                              </h6>
                            </li>
                            <ul className="list-unstyled mb-2">
                              <li>
                                This project is based on pickup and delivery
                                products mobile app. My role is as backened
                                developer for apis in Fastify.js, and create
                                admin section in ReactJS as frontend developer.
                              </li>
                            </ul>
                          </ul>

                          <h4 className="resume-timeline-item-desc-heading font-weight-bold">
                            Additional:
                          </h4>
                          <ul className="list-unstyled mb-2">
                            <li className="mb-2">
                              <h6 className>
                                <a
                                  href="https://www.npmjs.com/package/react-image-text-animation"
                                  target="blank"
                                >
                                  NPM Library
                                </a>
                              </h6>
                            </li>
                            <ul className="list-unstyled mb-2">
                              <li>
                                created an npm library for text transition.
                              </li>
                            </ul>
                            <li className="mb-2">
                              <h6 className>Chrome Extention</h6>
                            </li>
                            <ul className="list-unstyled mb-2">
                              <li>
                                have created an extension in React JS to check
                                ranking of the company (up to 100).
                              </li>
                            </ul>
                          </ul>
                          <h4 className="resume-timeline-item-desc-heading font-weight-bold">
                            Other Activity:
                          </h4>
                          <ul className="list-unstyled mb-2">
                            <li>
                              <h6>Technical Blogs</h6>
                            </li>
                            <ul className="list-inline">
                              <li className="list-inline-item">
                                <a
                                  href="https://medium.com/@bhoomi_/best-hybrid-app-development-for-2020-67be8c99176e"
                                  target="blank"
                                >
                                  <span className="badge badge-primary badge-pill">
                                    Best Hybrid App Development for 2020
                                  </span>
                                </a>
                              </li>
                              <li className="list-inline-item">
                                <a
                                  href="https://medium.com/@bhoomi_/react-native-is-the-future-of-mobile-development-495f74d2966"
                                  target="blank"
                                >
                                  <span className="badge badge-primary badge-pill">
                                    React Native is the future of Mobile
                                    Development{" "}
                                  </span>
                                </a>
                              </li>
                              {/* <li className="list-inline-item">
                                <a
                                  href="https://medium.com/@bhoomi_/useful-tips-and-tricks-to-make-your-small-business-website-a-big-hit-5b0b551ed11a"
                                  target="blank"
                                >
                                  <span className="badge badge-primary badge-pill">
                                    Useful Tips and Tricks to Make Your Small
                                    Business Website a Big Hit{" "}
                                  </span>
                                </a>
                              </li> */}
                              <li className="list-inline-item">
                                <a
                                  href="https://medium.com/@bhoomi_/how-the-react-js-is-changing-front-end-development-523dadfc5801"
                                  target="blank"
                                >
                                  <span className="badge badge-primary badge-pill">
                                    How The React JS is Changing Front-End
                                    Development
                                  </span>
                                </a>
                              </li>

                              <li className="list-inline-item">
                                <a
                                  href="https://deorwine.com/blog/best-ui-frameworks-for-your-new-react-js-app/"
                                  target="blank"
                                >
                                  <span className="badge badge-primary badge-pill">
                                    Best UI Frameworks for your new React.js App
                                  </span>
                                </a>
                              </li>
                              <li className="list-inline-item">
                                <a
                                  href="https://deorwine.com/blog/what-are-the-differences-between-reactjs-and-react-native/"
                                  target="blank"
                                >
                                  <span className="badge badge-primary badge-pill">
                                    What are the differences between ReactJS and
                                    React-Native?{" "}
                                  </span>
                                </a>
                              </li>
                              <li className="list-inline-item">
                                <a
                                  href="https://deorwine.com/blog/how-the-react-js-is-changing-front-end-development/"
                                  target="blank"
                                >
                                  <span className="badge badge-primary badge-pill">
                                    How The React JS is Changing Front-End
                                    Development
                                  </span>
                                </a>
                              </li>
                            </ul>
                          </ul>
                          {/* <h4 className="resume-timeline-item-desc-heading font-weight-bold">
                            Technologies used:
                          </h4>
                          <ul className="list-inline">
                            <li className="list-inline-item">
                              <span className="badge badge-primary badge-pill">
                                React JS
                              </span>
                            </li>
                            <li className="list-inline-item">
                              <span className="badge badge-primary badge-pill">
                                Redux
                              </span>
                            </li>
                            <li className="list-inline-item">
                              <span className="badge badge-primary badge-pill">
                                Next JS
                              </span>
                            </li>
                            <li className="list-inline-item">
                              <span className="badge badge-primary badge-pill">
                                Javascript
                              </span>
                            </li>
                            <li className="list-inline-item">
                              <span className="badge badge-primary badge-pill">
                                ES6
                              </span>
                            </li>
                            <li className="list-inline-item">
                              <span className="badge badge-primary badge-pill">
                                Redux
                              </span>
                            </li>
                            <li className="list-inline-item">
                              <span className="badge badge-primary badge-pill">
                                SailsJS
                              </span>
                            </li>
                            <li className="list-inline-item">
                              <span className="badge badge-primary badge-pill">
                                PWA
                              </span>
                            </li>
                            <li className="list-inline-item">
                              <span className="badge badge-primary badge-pill">
                                Fastify.js
                              </span>
                            </li>
                            
                          </ul> */}
                        </div>
                        {/*//resume-timeline-item-desc*/}
                      </article>

                      {/*//resume-timeline-item*/}
                    </div>
                    {/*//resume-timeline*/}
                  </div>
                </section>
                {/*//experience-section*/}
              </div>
              <div className="col-lg-3 col-sm-3">
                <section className="resume-section skills-section mb-5">
                  <h2 className="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">
                    Skills &amp; Tools
                  </h2>
                  <div className="resume-section-content">
                    <div className="resume-skill-item">
                      <h4 className="resume-skills-cat font-weight-bold">
                        Technologies
                      </h4>
                      <ul className="list-unstyled mb-2">
                        <li className="mb-2">
                          <div className="resume-skill-name">React Js</div>
                          {/* <div className="progress resume-progress">
                            <div
                              className="progress-bar theme-progress-bar-dark"
                              role="progressbar"
                              style={{ width: "95%" }}
                              aria-valuenow={25}
                              aria-valuemin={0}
                              aria-valuemax={100}
                            >95%</div>
                          </div> */}
                        </li>
                        <li className="mb-2">
                          <div className="resume-skill-name">Redux</div>
                          {/* <div className="progress resume-progress">
                            <div
                              className="progress-bar theme-progress-bar-dark"
                              role="progressbar"
                              style={{ width: "80%" }}
                              aria-valuenow={25}
                              aria-valuemin={0}
                              aria-valuemax={100}
                            />
                          </div> */}
                        </li>
                        <li className="mb-2">
                          <div className="resume-skill-name">Next Js</div>
                          {/* <div className="progress resume-progress">
                            <div
                              className="progress-bar theme-progress-bar-dark"
                              role="progressbar"
                              style={{ width: "98%" }}
                              aria-valuenow={25}
                              aria-valuemin={0}
                              aria-valuemax={100}
                            />
                          </div> */}
                        </li>
                        <li className="mb-2">
                          <div className="resume-skill-name">ES6</div>
                          {/* <div className="progress resume-progress">
                            <div
                              className="progress-bar theme-progress-bar-dark"
                              role="progressbar"
                              style={{ width: "90%" }}
                              aria-valuenow={25}
                              aria-valuemin={0}
                              aria-valuemax={100}
                            />
                          </div> */}
                        </li>
                        <li className="mb-2">
                          <div className="resume-skill-name">JavaScript</div>
                          {/* <div className="progress resume-progress">
                            <div
                              className="progress-bar theme-progress-bar-dark"
                              role="progressbar"
                              style={{ width: "96%" }}
                              aria-valuenow={25}
                              aria-valuemin={0}
                              aria-valuemax={100}
                            />
                          </div> */}
                        </li>

                        {/* <li className="mb-2">
                          <div className="resume-skill-name">GraphQL</div>
                          <div className="progress resume-progress">
                            <div
                              className="progress-bar theme-progress-bar-dark"
                              role="progressbar"
                              style={{ width: "50%" }}
                              aria-valuenow={25}
                              aria-valuemin={0}
                              aria-valuemax={100}
                            />
                          </div>
                        </li> */}
                        <li className="mb-2">
                          <div className="resume-skill-name">Node.js</div>
                          {/* <div className="progress resume-progress">
                            <div
                              className="progress-bar theme-progress-bar-dark"
                              role="progressbar"
                              style={{ width: "40%" }}
                              aria-valuenow={35}
                              aria-valuemin={0}
                              aria-valuemax={100}
                            />
                          </div> */}
                        </li>
                        <li className="mb-2">
                          <div className="resume-skill-name">MySql</div>
                          {/* <div className="progress resume-progress">
                            <div
                              className="progress-bar theme-progress-bar-dark"
                              role="progressbar"
                              style={{ width: "40%" }}
                              aria-valuenow={35}
                              aria-valuemin={0}
                              aria-valuemax={100}
                            />
                          </div> */}
                        </li>

                        <li className="mb-2">
                          <div className="resume-skill-name">SailsJS</div>
                          {/* <div className="progress resume-progress">
                            <div
                              className="progress-bar theme-progress-bar-dark"
                              role="progressbar"
                              style={{ width: "20%" }}
                              aria-valuenow={25}
                              aria-valuemin={0}
                              aria-valuemax={100}
                            />
                          </div> */}
                        </li>
                        <li className="mb-2">
                          <div className="resume-skill-name">PWA</div>
                          {/* <div className="progress resume-progress">
                            <div
                              className="progress-bar theme-progress-bar-dark"
                              role="progressbar"
                              style={{ width: "25%" }}
                              aria-valuenow={25}
                              aria-valuemin={0}
                              aria-valuemax={100}
                            />
                          </div> */}
                        </li>
                        <li className="mb-2">
                          <div className="resume-skill-name">Fastify.js</div>
                        </li>
                      </ul>
                    </div>
                    {/*//resume-skill-item*/}
                    {/* <div class="resume-skill-item">
									<h4 class="resume-skills-cat font-weight-bold">Backend</h4>
									<ul class="list-unstyled">
										<li class="mb-2">
											<div class="resume-skill-name">Python/Django</div>
											<div class="progress resume-progress">
												<div class="progress-bar theme-progress-bar-dark" role="progressbar"
													style="width: 95%" aria-valuenow="25" aria-valuemin="0"
													aria-valuemax="100"></div>
											</div>
										</li>
										<li class="mb-2">
											<div class="resume-skill-name">Ruby/Rails</div>
											<div class="progress resume-progress">
												<div class="progress-bar theme-progress-bar-dark" role="progressbar"
													style="width: 92%" aria-valuenow="25" aria-valuemin="0"
													aria-valuemax="100"></div>
											</div>
										</li>
										<li class="mb-2">
											<div class="resume-skill-name">PHP</div>
											<div class="progress resume-progress">
												<div class="progress-bar theme-progress-bar-dark" role="progressbar"
													style="width: 86%" aria-valuenow="25" aria-valuemin="0"
													aria-valuemax="100"></div>
											</div>
										</li>
										<li class="mb-2">
											<div class="resume-skill-name">WordPress/Shopify</div>
											<div class="progress resume-progress">
												<div class="progress-bar theme-progress-bar-dark" role="progressbar"
													style="width: 82%" aria-valuenow="25" aria-valuemin="0"
													aria-valuemax="100"></div>
											</div>
										</li>
									</ul>
								</div> */}
                    {/*//resume-skill-item*/}
                    {/* <div className="resume-skill-item">
                      <h4 className="resume-skills-cat font-weight-bold">
                        Others
                      </h4>
                      <ul className="list-inline">
                        <li className="list-inline-item">
                          <span className="badge badge-light">Code Review</span>
                        </li>
                        <li className="list-inline-item">
                          <span className="badge badge-light">
                            Git/BitBucket
                          </span>
                        </li>
                        <li className="list-inline-item">
                          <span className="badge badge-light">
                            Technical Blogs
                          </span>
                        </li>
                      </ul>
                    </div> */}
                    {/*//resume-skill-item*/}
                  </div>
                  {/*resume-section-content*/}
                </section>
                {/*//skills-section*/}
                <section className="resume-section education-section mb-5">
                  <h2 className="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">
                    Education
                  </h2>
                  <div className="resume-section-content">
                    <ul className="list-unstyled">
                      <li className="mb-2">
                        <div className="resume-degree font-weight-bold">
                          MCA{" "}
                        </div>
                        <div className="resume-degree-org">
                          Jaipur National University
                        </div>
                        <div className="resume-degree-time">2014 - 2017</div>
                      </li>
                      <li>
                        <div className="resume-degree font-weight-bold">
                          BSc Maths Physics Chemistry
                        </div>
                        <div className="resume-degree-org">
                          R.D. Girls College, Bharatpur
                        </div>
                        <div className="resume-degree-time">2010 - 2013</div>
                      </li>
                    </ul>
                  </div>
                </section>
                {/*//education-section*/}
                {/* <section class="resume-section reference-section mb-5">
							<h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">Awards</h2>
							<div class="resume-section-content">
								<ul class="list-unstyled resume-awards-list">
									<li class="mb-2 pl-4 position-relative">
										<i class="resume-award-icon fas fa-trophy position-absolute"
											data-fa-transform="shrink-2"></i>
										<div class="resume-award-name">Award Name Lorem</div>
										<div class="resume-award-desc">Award desc goes here, ultricies nec, pellentesque
											eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo.
										</div>
									</li>
									<li class="mb-0 pl-4 position-relative">
										<i class="resume-award-icon fas fa-trophy position-absolute"
											data-fa-transform="shrink-2"></i>
										<div class="resume-award-name">Award Name Ipsum</div>
										<div class="resume-award-desc">Award desc goes here, ultricies nec,
											pellentesque.</div>
									</li>
								</ul>
							</div>
						</section> */}
                {/*//interests-section*/}
                <section className="resume-section language-section mb-5">
                  <h2 className="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">
                    Language
                  </h2>
                  <div className="resume-section-content">
                    <ul className="list-unstyled resume-lang-list">
                      <li className="mb-2">
                        <span className="resume-lang-name font-weight-bold">
                          Hindi
                        </span>
                        <small className="text-muted font-weight-normal">
                          (Native)
                        </small>
                      </li>
                      <li className="mb-2 align-middle">
                        <span className="resume-lang-name font-weight-bold">
                          Eglish
                        </span>{" "}
                        <small className="text-muted font-weight-normal">
                          (Professional)
                        </small>
                      </li>
                    </ul>
                  </div>
                </section>
                {/*//language-section*/}
                <section className="resume-section interests-section mb-5">
                  <h2 className="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">
                    Interests
                  </h2>
                  <div className="resume-section-content">
                    <ul className="list-unstyled d-inline-flex">
                      <li className="mb-1">Reading, </li>
                      <li className="mb-1 ml-1">Coding</li>
                    </ul>
                  </div>
                </section>
                {/*//interests-section*/}
              </div>
            </div>
            {/*//row*/}
          </div>
          {/*//resume-body*/}
        </div>
      </article>
      {/* <footer className="footer text-center pt-2 pb-5">
       
        <small className="copyright">
          Designed with <i className="fas fa-heart" /> by{" "}
          <a href="http://themes.3rdwavemedia.com" target="_blank">
            Xiaoying Riley
          </a>{" "}
          for developers
        </small>
      </footer> */}
    </div>
  );
}

export default App;
